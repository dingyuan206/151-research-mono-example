# Example Mono Repo

## Front-ends

* [React](portal-react/README.md)

## Back-ends

* [Flask](api-flask/README.md)