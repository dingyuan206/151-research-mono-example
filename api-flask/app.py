from flask import Flask, jsonify

app = Flask(__name__)

from controllers import hello_world

@app.route('/', methods=['GET'])
def index():
    return jsonify(hello_world())

if __name__ == '__main__':
    app.run()

